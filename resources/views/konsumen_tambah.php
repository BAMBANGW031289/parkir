<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Parkiran </title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    Data Konsumen - <strong>TAMBAH DATA</strong>
                </div>
                <div class="card-body">
                    <a href="/konsumen" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>

                    <form method="post" action="/konsumen/store">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nama Konsumen</label>
                            <input type="text" name="konsumen" class="form-control" placeholder="Nama konsumen ..">

                            @if($errors->has('konsumen'))
                                <div class="text-danger">
                                    {{ $errors->first('konsumen')}}
                                </div>
                            @endif

                        </div>


                        <div class="form-group">
                            <label>jenis kendaraan</label>
                            <input type="text" name="jenis_kendaraan" class="form-control" placeholder="jenis_kendaraan ..">

                            @if($errors->has('jenis_kendaraan'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis_kendaraan')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>No Polisi</label>
                            <input type="text" name="no_polisi" class="form-control" placeholder="Nopol ..">

                            @if($errors->has('no_polisi'))
                                <div class="text-danger">
                                    {{ $errors->first('no_polisi')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>tanggal lahir</label>
                            <input type="text" name="tgl_lahir" class="form-control" placeholder="tanggal lahir ..">

                            @if($errors->has('tgl_lahir'))
                                <div class="text-danger">
                                    {{ $errors->first('tgl_lahir')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>kelamin</label>
                            <input type="text" name="kelamin" class="form-control" placeholder="kelamin ..">

                            @if($errors->has('kelamin'))
                                <div class="text-danger">
                                    {{ $errors->first('kelamin')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>no_hp</label>
                            <input type="text" name="no_hp" class="form-control" placeholder="Nama pegawai ..">

                            @if($errors->has('no_hp'))
                                <div class="text-danger">
                                    {{ $errors->first('no_hp')}}
                                </div>
                            @endif

                        </div>








                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </body>
</html>
