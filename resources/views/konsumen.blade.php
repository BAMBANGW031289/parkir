<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Parkiran</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data konsumen 
                </div>
                <div class="card-body">
                    <a href="/konsumen/tambah" class="btn btn-primary">Input konsumen Baru</a>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>konsumen</th>
                                <th>jenis kendaraan</th>
                                <th>no polisi</th>
                                <th>tgl_lahir</th>
                                <th>kelamin</th>
                                <th>no_hp</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($konsumen as $p)
                            <tr>
                                <td>{{ $p->konsumen }}</td>
                                <td>{{ $p->jenis_kendaraan }}</td>
                                <td>{{ $p->no_polisi }}</td>
                                <td>{{ $p->tgl_lahir }}</td>
                                <td>{{ $p->kelamin }}</td>
                                <td>{{ $p->no_hp }}</td>
                                <td>
                                    <a href="/konsumen/edit/{{ $p->id }}" class="btn btn-warning">Edit</a>
                                    <a href="/konsumen/hapus/{{ $p->id }}" class="btn btn-danger">Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
