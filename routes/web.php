<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/konsumen', 'KonsumenController@index');
Route::get('/konsumen/tambah', 'KonsumenController@tambah');
Route::post('/konsumen/store', 'KonsumenController@store');
Route::post('/konsumen/store', 'KonsumenController@store');
Route::get('/konsumen/edit/{id}', 'KonsumenController@edit');
