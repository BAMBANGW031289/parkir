<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Konsumen;

class KonsumenController extends Controller
{

    public function index()
    {
    	$konsumen = Konsumen::all();
    	return view('konsumen', ['konsumen' => $konsumen]);
    }

    public function tambah()
    {
    	return view('konsumen_tambah');
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'konsumen' => 'required',
    		'jenis_kendaraan' => 'required',
        'no_polisi' => 'required',
    		'tgl_lahir' => 'required',
        'kelamin' => 'required',
    		'no_hp' => 'required'
    	]);

        Konsumen::create([
    		'konsumen' => $request->konsumen,
    		'jenis_kendaraan' => $request->jenis_kendaraan,
        'no_polisi' => $request->no_polisi,
    		'tgl_lahir' => $request->tgl_lahir,
        'kelamin' => $request->kelamin,
    		'no_hp' => $request->no_hp

    	]);

    	return redirect('/konsumen');
    }


}
